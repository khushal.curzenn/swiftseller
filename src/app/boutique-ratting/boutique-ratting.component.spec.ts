import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoutiqueRattingComponent } from './boutique-ratting.component';

describe('BoutiqueRattingComponent', () => {
  let component: BoutiqueRattingComponent;
  let fixture: ComponentFixture<BoutiqueRattingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoutiqueRattingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BoutiqueRattingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
