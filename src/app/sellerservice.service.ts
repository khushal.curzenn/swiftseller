import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SellerserviceService {

  hosturl = "http://localhost:4201/";
  // baseapiurl = "http://localhost:9003/admin-panel/";
  // baseapiurl2 = "http://localhost:9003/";

  // hosturl = "https://vendeur.swift-fashion.com/";
    baseapiurl="https://mainserver.swift-fashion.com/admin-panel/";
    baseapiurl2="https://mainserver.swift-fashion.com/";
  main_id: any; token: any; email: any; sessioin: any
  constructor() {
    this.main_id = localStorage.getItem('main_sellerid')
    this.token = localStorage.getItem('token')
    this.email = localStorage.getItem('selleremail')
    this.sessioin = localStorage.getItem('loginsession')

    if (this.sessioin >= '24') {
      setTimeout(() => {
        localStorage.clear()
        alert("Your session timeout")
        window.location.reload()
      }, 1000 * 60 * 60 *24 );
    }

  }
}
