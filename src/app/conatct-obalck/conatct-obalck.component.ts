import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-conatct-obalck',
  templateUrl: './conatct-obalck.component.html',
  styleUrls: ['./conatct-obalck.component.css']
})
export class ConatctObalckComponent implements OnInit {
  registerform!: FormGroup
  image: any
  main_id:any;
  myFiles:any
  formData:any
  baseurl:any
  fulldata:any
  constructor(private seller:SellerserviceService, private http:HttpClient,) { 
    this.main_id = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2
  }

  ngOnInit(): void {
    this.registerform = new FormGroup({

      request_purpose: new FormControl('', Validators.required),
      reason: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required),
     // seller_id
     // photo
    })
  }
  
  onFileUpdate(event: any) {
    this.myFiles = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
    console.log(this.myFiles)
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.image = reader.result
        //console.log(reader.result );
    };
  }
  register() {
    this.formData = new FormData();
    this.formData.append('request_purpose', this.registerform.value.request_purpose);
    this.formData.append('reason', this.registerform.value.reason);
    this.formData.append('message', this.registerform.value.message);
   
    this.formData.append('seller_id', this.main_id);
    for (var i = 0; i < this.myFiles.length; i++) {
      this.formData.append("photo", this.myFiles[i]);
    }

    if( this.registerform.valid){
      console.log(this.formData)

      this.http.post(this.baseurl + "api/seller/submitSellerQuery", this.formData).subscribe(res => {
        this.fulldata = res
        console.log(this.fulldata)
        if (this.fulldata.status == true) {
          this.registerform.reset()
          window.location.reload()
        }
      })
    }else{
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }
  }
}
