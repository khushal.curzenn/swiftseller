import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';




@Component({
  selector: 'app-edit-shopprofile',
  templateUrl: './edit-shopprofile.component.html',
  styleUrls: ['./edit-shopprofile.component.css']
})
export class EditShopprofileComponent implements OnInit {
  registerform!: FormGroup
  main_id: any; main_email: any
  baseurl: any
  profile: any
  myFiles: any
  formData: any;
  getstatus: any; endAddress: any; endLatitude: any; endLongitude: any
  mypickaddress: any; image: any
  thisImage: any
 
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.main_email = localStorage.getItem('selleremail')
    this.baseurl = seller.baseapiurl2
    this.registerform = new FormGroup({
      shopname: new FormControl(''),
      pickupaddress: new FormControl(''),
      shopdesc: new FormControl(''),

    })
    this.mypickaddress = {

    }

    
  }

  ngOnInit(): void {
    this.http.get(this.baseurl + "api/seller/getuserprofile/" + this.main_id).subscribe(res => {
      this.profile = res
      this.image = this.baseurl + "static/public/" + this.profile.data.shopphoto
      // this.mypickaddress.address = this.profile.data.pickupaddress[0].address
      // this.mypickaddress.latlong = this.profile.data.pickupaddress[0].latlong
     
     
      this.registerform = new FormGroup({
        shopname: new FormControl(this.profile.data.shopname),
        //pickupaddress: new FormControl( ),
        shopdesc: new FormControl(this.profile.data.shopdesc),

      })
    })
  }
  onFileUpdate(event: any) {
    this.myFiles = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
    console.log(this.myFiles)

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.image = reader.result
      //console.log(reader.result );
    };
  }
  handleAddressChange2(address2: any) {

    this.endAddress = address2.formatted_address
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
    this.mypickaddress.address = this.endAddress
    this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]
   


  }
  Submitprofile() {
   
    this.registerform.value.pickupaddress = this.mypickaddress 
   
    this.formData = new FormData();
    this.formData.append('shopname', this.registerform.value.shopname);
    this.formData.append('pickupaddress', JSON.stringify(this.registerform.value.pickupaddress));
    this.formData.append('shopdesc', this.registerform.value.shopdesc);
    this.formData.append('id', this.main_id);
    this.formData.append('email', this.main_email);

    if (this.myFiles != undefined) {
      for (var i = 0; i < this.myFiles.length; i++) {
        this.formData.append("shopphoto", this.myFiles[i]);
      }
    }


    this.http.post(this.baseurl + "api/seller/createseller", this.formData).subscribe(res => {
      console.log(res)
      this.getstatus = res
      if (this.getstatus.status == true) {
        // alert(this.getstatus.message)
        window.location.reload()

      }
    })
  }



}
