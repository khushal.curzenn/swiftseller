import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  openSidebar: boolean = true;
  currentUrlSegment: any
  host:any

  menuSidebar = [
    {
      link_name: "Tableau de bord",
      link: "/dashboard",
      icon: "fa-solid fa-dashboard",
      sub_menu: [
      
      ]
    },
    {
      link_name: "Expédition",
      link: null,
      icon: "fa-solid fa-truck-fast",
      sub_menu: [
        {
          link_name: "Mon expédition",
          link: "shipment",
        },
        // {
        //   link_name: "Commandes à expédier",
        //   link: "ordertoship",
        // }
      ]
    }, {
      link_name: "Commandez",
      link: null,
      icon: "fa-brands fa-first-order-alt",
      sub_menu: [
        {
          link_name: " Mes commandes",
          link: "myorders",
        },
        // {
        //   link_name: " Annulation",
        //   link: "/",
        // }, {
        //   link_name: " Retour/Remboursement",
        //   link: "/",
        // }
      ]
    },
    {
      link_name: " Produits",
      link: null,
      icon: "fa-brands fa-product-hunt",
      sub_menu: [
        {
          link_name: "  Mes produits",
          link: "myproducts",
        }, {
          link_name: "  Ajouter de nouveaux produits",
          link: "add_Product",
        },
        //  {
        //   link_name: "  Violations des produits",
        //   link: "/",
        // }
      ]
    }
    ,
    {
      link_name: "  Finances",
      link: null,
      icon: "fa-solid fa-money-bill",
      sub_menu: [
        {
          link_name: " Mes revenus",
          link: "my_income",
        }, {
          link_name: " Mon solde ",
          link: "my_balance",
        }
        // , {
        //   link_name: "Compte bancaire",
        //   link: "my_account",
        // }
      ]
    },
    // {
    //   link_name: " Données",
    //   link: null,
    //   icon: "fa-solid fa-database",
    //   sub_menu: [
    //     {
    //       link_name: " Perspectives d'affaires",
    //       link: "all_data",
    //     }
    //   ]
    // },
    {
      link_name: " Service à la clientèle",
      link: null,
      icon: "fa-solid fa-headset",
      sub_menu: [
        {
          link_name: "Messages",
          link: "messeges",
        },

      ]
    },
    {
      link_name: "Boutique",
      link: null,
      icon: "fa-solid fa-shop",
      sub_menu: [
        {
          link_name: "  Évaluation de la boutique",
          link: "shop_review",
        }
        // ,
        // {
        //   link_name: " Profil de la boutique ",
        //   link: "shop_profile2",
        // }
        ,
        {
          link_name: "Compte",
          link: "your_profile",
        },
        {
          link_name: "Notifications",
          link: "notifications",
        },
        {
          link_name: "Paramètres",
          link: "parameters",
        },
        // {
        //   link_name: "À propos de Swift",
        //   link: "about",
        //   ref:'_blank'
        // },
        // {
        //   link_name: "Contactez nous",
        //   link: "contact",
        // },
        // {
        //   link_name: "Conditions générales de vente ",
        //   link: "termsandconditions",
        //   ref:'_blank'
        // },
        // {
        //   link_name: "Vos informations personnelles",
        //   link: "your_personal",
        //   ref:'_blank'
        // },
        

      ]
    },
    {
      link_name: " Centre de marketing",
      link: null,
      icon: "fa-solid fa-door-open",
      sub_menu: [
        {
          link_name: " Coupon de réduction",
          link: "coupon",
        }
      ]
    },
    {
      link_name: "Disponibilité",
      link: null,
      icon: "fa-solid fa-door-open",
      sub_menu: [
        {
          link_name: "Ajouter",
          link: "manageSchedule",
        },
        {
          link_name: "Tous",
          link: "getSchedule",
        }
      ]
    },
    {
      link_name: "Branche",
      link: null,
      icon: "fa-solid fa-door-open",
      sub_menu: [
        {
          link_name: "Ajouter une branche",
          link: "addBranch",
        },
        {
          link_name: "Toutes les branches",
          link: "allBranch",
        }
      ]
    }, 
    {
      link_name: "Document Stripe",
      link: null,
      icon: "fa-solid fa-door-open",
      sub_menu: [
        {
          link_name: "Télécharger",
          link: "uploadStripeDocument",
        } 
      ]
    } 
    // ,
    // {
    //   link_name: "Forfait d'abonnement",
    //   link: null,
    //   icon: "fas fa-money-check-alt",
    //   sub_menu: [
    //     {
    //       link_name: " Forfait d'abonnement",
    //       link: "my_sub_package",
    //     }
    //   ]
    // }
    // {
    //   link_name: " Réglage de",
    //   link: null,
    //   icon: "fa-sharp fa-solid fa-gear",
    //   sub_menu: [
    //     {
    //       link_name: "  Mes adresses",
    //       link: "/",
    //     },
    //     {
    //       link_name: "  Paramètres de la boutique ",
    //       link: "/",
    //     },
    //     {
    //       link_name: "  Compte",
    //       link: "/",
    //     }
    //   ]
    // }
  ]
  constructor(private seller:SellerserviceService) {
    this.host = seller.hosturl
   }

  ngOnInit(): void {
  }

  showSubmenu(itemEl: HTMLElement) {

    itemEl.classList.toggle("showMenu");
  }

}
