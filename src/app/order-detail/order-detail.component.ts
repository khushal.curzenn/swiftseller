import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  baseurl:any; orderID:any; mysingleOrder:any
  getorderdetailbyorderid:any; Apires:any
  order_products:any
  addNote:any ; addNoteRes:any
  noteform !: FormGroup
  constructor(private route:ActivatedRoute,private seller: SellerserviceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.orderID = this.route.snapshot.queryParams["id"]
    this.getorderdetailbyorderid = this.baseurl+"api/order/getorderdetailbyorderid/"+this.orderID
    this.addNote = this.baseurl+"api/seller/addNote"

    this.noteform = new FormGroup({
     
      message: new FormControl('',  Validators.required),

    })
   }

  ngOnInit(): void {

    this.http.get(this.getorderdetailbyorderid).subscribe(res=>{
      this.Apires = res
      if(this.Apires.status){
        this.mysingleOrder = this.Apires.data[0]
        this.order_products = this.mysingleOrder.sellerinfo
        console.log(   this.Apires, "this.mysingleOrder")
      }
    })
    
  }
  send(){
   
    this.noteform.value.order_id = this.orderID
    console.log(this.noteform.value)
    if(this.noteform.valid ){
      this.http.post(this.addNote,  this.noteform.value).subscribe(res=>{
        this.addNoteRes =  res
        if(this.addNoteRes.status){
          window.location.reload()
        }
      })
    }else{

      for (const control of Object.keys(this.noteform.controls)) {
        this.noteform.controls[control].markAsTouched();
      }
      return;
     
    }
  }

}
