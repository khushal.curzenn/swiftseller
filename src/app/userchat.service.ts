import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { io } from "socket.io-client";
@Injectable({
  providedIn: 'root'
})
export class UserchatService {

  
   
  public message$: BehaviorSubject<any> = new BehaviorSubject('');
  constructor() {}

  socket = io('https://mainserver.swift-fashion.com/');

  public sendMessage(message: any) {
    //console.log('sendMessage: ', message)
    this.socket.emit('sendMessage', message);
    

  }

  public getNewMessage = () => {
    this.socket.on('getResponseInitate', (response) =>{
      console.log(response);
      this.message$.next(response);
    });

    return this.message$.asObservable();
  };
}
