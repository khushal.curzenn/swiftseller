import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl, FormControl, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CategoriesComponent } from '../categories/categories.component';


@Component({
  selector: 'app-edit-variation',
  templateUrl: './edit-variation.component.html',
  styleUrls: ['./edit-variation.component.css']
})
export class EditVariationComponent implements OnInit {

  allpics: any;apiResponse:any;base_url:string=''; apiResponse_2:any;
  userForm!: FormGroup;
  phone: any;
  public products = [
    {
      id: 1,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 2,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 3,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 4,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 5,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 6,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 7,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 8,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 9,
      imgUrl: "",
      imgBase64Data: ""
    }
  ];
  url: any
  format: any
  baseurl: any;

  base_url_front:any;
  token: any
  user_type: any
  productarray: any
  product: any
  form!: FormGroup
  addSection5!: FormGroup
  main_id: any
  formBuilder: any;
  myFiles: any;

  myFilesNew0:any; myFilesNew1:any; myFilesNew2:any; myFilesNew3:any; myFilesNew4:any;
  myFilesNew5:any; myFilesNew6:any; myFilesNew7:any; myFilesNew8:any; myFilesNew9:any;
  myFilesNew10:any; myFilesNew11:any; myFilesNew12:any; myFilesNew13:any; myFilesNew14:any;
  myFilesNew15:any; myFilesNew16:any; myFilesNew17:any; myFilesNew18:any; myFilesNew19:any;

  firstMainVariationVal:string='';secondMainVariationVal:string='';
  firstSubVariationArray :any;secondSubVariationArray:any;
  
  priceVariation0:any;priceVariation1:any;priceVariation2:any;priceVariation3:any;

  mydispatch_info: any
  mysize: any
  width: any; length: any; height: any; weight: any; getshippingMethodsByweight: any
  recordsship: any; weightval: any; allCategories: any; parentCategories: any;subparentCategories: any
  simplecontent:any; variablecontent:any;myvariation:any;myProperty:any;abc:any
  mytestarray:any;
  productForm!: FormGroup;
  variationNameOne:any;subVariationNameOne:any;
  variationNameTwo:any;subVariationNameTwo:any;
  formData:any;
  old_attr_price:any;old_attr_stock:any;old_attr_SKU:any;id:any;
  add_quantity_number:number = 1;
  add_quantity_monday_number:number = 1;
  color_val:string="Couleur";
  record:any;image_length:number=0;old_description:string="";old_catagory_id:any;old_dangerous_goods:string="";old_weight:number=0;old_width:number=0;old_length:number=0;old_height:number=0;old_seller_will_bear_shipping:boolean=false;old_condition:any;
  old_sku:string="";old_price:number=0;old_stock:number=0;
  
  all_size:any; all_brand:any; all_style:any;  all_color:any;
  old_variation_option_2:any;
  loading:any; regdis:any;
  dropdownListSize : any[] = [];

  constructor(
    private fb: FormBuilder, private seller: SellerserviceService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router,
    public dialog: MatDialog,private route: ActivatedRoute ) {
      this.myFilesNew0 = [];this.myFilesNew1 = [];this.myFilesNew2 = [];this.myFilesNew3 = [];this.myFilesNew4 = [];this.myFilesNew5 = [];this.myFilesNew6 = [];this.myFilesNew7 = [];this.myFilesNew8 = [];this.myFilesNew9 = [];

      this.priceVariation0 = [];this.priceVariation1 = [];
      this.priceVariation2 = [];this.priceVariation3 = [];


      this.firstSubVariationArray = [];this.secondSubVariationArray = [];
      this.myFilesNew10 = [];this.myFilesNew11 = [];this.myFilesNew12 = [];this.myFilesNew13 = [];this.myFilesNew14 = [];this.myFilesNew15 = [];this.myFilesNew16 = [];this.myFilesNew17 = [];this.myFilesNew18 = [];this.myFilesNew19 = [];
    this.main_id = localStorage.getItem('main_sellerid')

    this.base_url_front = seller.hosturl;
    this.baseurl = seller.baseapiurl2
    this.base_url = seller.baseapiurl2;
    this.productarray = []
    this.product = []
    this.allpics = []
    this.myFiles = []
    this.subparentCategories  =[]
    this.allCategories = []
    this.mydispatch_info = {}
    this.mysize = {}
    this.simplecontent = false
    this.variablecontent = true
    this.myvariation = 'color'
    this.myProperty = ''
    this.getshippingMethodsByweight = []
    this.parentCategories = []
    this.mytestarray = []

    this.loading = false
    this.regdis = false

    this.userForm = this.fb.group({
     
      phones: this.fb.array([{
        name:this.fb.control(null)
    }]),
      phones2: this.fb.array([this.fb.control(null)
      ]),
      
    })
    this.addSection5 = new FormGroup({
      parent_sku: new FormControl('SKU'),
      condition: new FormControl(),
      preorder: new FormControl(true),
      shippingtime: new FormControl("in days"),
    })
    this.form = new FormGroup({
      sku: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      price: new FormControl(2000, [Validators.required]),
      catagory: new FormControl('Access', [Validators.required]),
      //photo: new FormControl([], [Validators.required]),
      dangerous_goods: new FormControl('', [Validators.required]),
      video: new FormControl('', [Validators.required]),
      attributes: new FormControl([
        {
          "variation_name": "isramthere",
          "variation_options": [
            {
              "option": "yes",
              "option_value": "yes"
            }

          ]
        }, {
          "variation_name": "ramsize",
          "variation_options": [
            {
              "option": "8gb",
              "option_value": "8gb"
            }

          ]
        }
      ], [Validators.required]),
      dispatch_info: new FormControl(),
      provider_id: new FormControl(this.main_id, [Validators.required]),
      weight: new FormControl(this.weightval),
      size: new FormControl(),


    });

    this.productForm = this.fb.group({
      firstMainVariation: new FormControl('', []),
      secondMainVariation: new FormControl('', []),
      product_id: '',
      //firstMainVariation: '',
      forAllVariationPrice: '',
      forAllVariationStock: '',
      forAllVariationSKU: '',

      //secondMainVariation: '',
      quantities: this.fb.array([]),
      quantitiesMonday: this.fb.array([]) ,
      quantitiesTuesday: this.fb.array([]) ,
    });
    if(this.route.snapshot.queryParams['id'] != "" && this.route.snapshot.queryParams['id'] != null)
    {
      this.id = this.route.snapshot.queryParams['id'];
    }
    console.log("this.id "+this.id);
    this.http.get(this.base_url+"api/product/getproductbyidtoedit/"+this.id).subscribe(response=>{
      this.apiResponse = response;
      console.log(response);
      this.apiResponse.message = "";
      if(this.apiResponse.status == true)
      {
        this.record = this.apiResponse.data;
        //console.log("this.record ", this.record);
        
        this.old_price=this.record.price;
        this.old_stock=this.record.stock;
        this.old_sku = this.record.sku;
        
        // image_length:number=0;old_description:string="";old_catagory_id:any;old_dangerous_goods:string="";
        //old_weight:number=0;old_width:number=0;old_length:number=0;old_height:number=0;old_seller_will_bear_shipping:boolean=false;old_condition:any;
        // old_title:string="";old_price:number=0;old_stock:number=0;old_enable_variation:boolean=false;
        this.apiResponse.message = "";
        this.apiResponse.status = "";
      }
      
    });
    this.addQuantity();

    // this.addQuantityMonday();
    // this.newQuantity2();
    // this.addQuantityMonday();
    // this.newQuantity2();
      
       this.http.get(this.base_url+"api/product/getAllProductColor").subscribe(res=>{
        console.log("res ", res);
        this.apiResponse = res;
        this.all_color = this.apiResponse.data;
       });
       this.http.get(this.base_url+"api/product/getAllProductSize").subscribe(res=>{
        console.log("res ", res);
        this.apiResponse = res;
        this.dropdownListSize = this.all_size = this.apiResponse.data;
        //this.dropdownListSize = this.all_size[0];
        // this.all_size.map((val:any)=>{
        //   let objj = {
        //     id: val._id,name:val.name
        //   }
        // });
        console.log("this.dropdownListSize  ", this.dropdownListSize );
       });
    
       this.http.get(this.base_url+"api/product/getSingleVariation/"+this.id).subscribe(response=>{
        this.apiResponse = response;
        //console.log(response);
        this.apiResponse.message = "";
        if(this.apiResponse.status == true)
        {
          this.record = this.apiResponse.data;
          //console.log("this.record ", this.record);
          if(this.record.length > 0)
          {
            this.old_variation_option_2 = this.record;
            for(let c=0; c<this.record.length; c++)
            {
              console.log("hereeeeeeeeeeeee 271 ");
              this.addQuantityTuesday();
            }
          }
          // this.old_variation_2 = this.record.variation_2;
          // this.old_variation_option_2 = this.record.variation_option_2;
          // this.old_array_price = this.record.price;
          // this.old_array_stock = this.record.stock;
          // this.old_array_sku = this.record.sku;
          // this.old_images = this.record.images;
          // this.old_variation_option_1 = this.record.variation_option_1;
          // for(let c=0; c<this.old_variation_option_2.length; c++)
          // {
          //   this.addQuantityTuesday();
          // } 
          // for(let c=0; c<this.old_images.length; c++)
          // {
          //   this.addQuantityWednesday();
          // }
          //addQuantityWednesday
          //addQuantityTuesday
  
          // console.log("this.old_variation_2 ", this.old_variation_2);
          // console.log("this.old_array_price ", this.old_array_price);
          // console.log("this.old_array_stock ", this.old_array_stock);
          // console.log("this.old_array_sku ", this.old_array_sku);
          // console.log("this.old_array_sku ==================>>>>> ");
          // console.log("this.old_images ", this.old_images);
  
        
          this.apiResponse.message = "";
          this.apiResponse.status = "";
        }
        
      });   
  }

  removeOldSecondVariation(ii:any )
  {
    //alert();
    console.log("ii ", ii);
    console.log("idd ", this.id); 
    if(confirm("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))
    {
      let qParam = {product_id:this.id,indexx:ii};
      this.http.post(this.base_url+"api/product/deleteAttributeFromId",qParam).subscribe((response:any)=>{
        // console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.status == true)
        {
          setTimeout(() => {
            window.location.reload();
          }, 1000); 
        }
      });
    }
  }


  images0 : string[] = []; images1 : string[] = []; images2 : string[] = [];
  images3 : string[] = []; images4 : string[] = []; images5 : string[] = [];
  images6 : string[] = []; images7 : string[] = []; images8 : string[] = [];
  images9 : string[] = []; images10 : string[] = []; images11 : string[] = [];
  images12 : string[] = []; images13 : string[] = []; images14 : string[] = [];
  images15 : string[] = []; images16 : string[] = []; images17 : string[] = [];
  images18 : string[] = [];  images19 : string[] = [];
  onFileChange(event:any,counter:number=0)
  {
    //console.log(counter);
    
    if(counter == 0)
    {
      
      for (var i = 0; i < event.target.files.length; i++)
      {
        //console.log("this.images0.length ",this.myFilesNew0.length);
        if(this.myFilesNew0.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string

            this.images0.push(event.target.result); 
            //this.patchValues();
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew0.push(event.target.files[i]);
        }
        
      }
    }
    
    if(counter == 1)
    {
      //images1
      //console.log("this.images1.length ",this.myFilesNew1.length);
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew1.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images1.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 

          this.myFilesNew1.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 2)
    {
      for (var i = 0; i < event.target.files.length; i++)
      { if(this.myFilesNew2.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images2.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew2.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 3)
    {
      for (var i = 0; i < event.target.files.length; i++)
      { if(this.myFilesNew3.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images3.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew3.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 4)
    {
      for (var i = 0; i < event.target.files.length; i++)
      { if(this.myFilesNew4.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images4.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew4.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 5)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew5.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images5.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew5.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 6)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew6.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images6.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]);
          this.myFilesNew6.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 7)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew7.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images7.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew7.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 8)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew8.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images8.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew8.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 9)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew9.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images9.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew9.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 10)
    {
      for (var i = 0; i < event.target.files.length; i++)
      { 
        if(this.myFilesNew10.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images10.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew10.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 11)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew11.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images11.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew11.push(event.target.files[i]);
        }
      }
    }

    
    if(counter == 12)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew12.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images12.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew12.push(event.target.files[i]);
        }
      }
    }
    
    if(counter == 13)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew13.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images13.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew13.push(event.target.files[i]);
        }
      }
    }
    
    if(counter == 14)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew14.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images14.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew14.push(event.target.files[i]);
        }
      }
    }
    
    if(counter == 15)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew15.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images15.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew15.push(event.target.files[i]);
        }
      }
    }
    
    if(counter == 16)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew16.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images16.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew16.push(event.target.files[i]);
        }
      }
    }
    
    if(counter == 17)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew17.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images17.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew17.push(event.target.files[i]);
        }
      }
    }
    
    if(counter == 18)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew18.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images18.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew18.push(event.target.files[i]);
        }
      }
    }
    
    if(counter == 19)
    {
      for (var i = 0; i < event.target.files.length; i++)
      {
        if(this.myFilesNew19.length <= 5)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images19.push(event.target.result);
          }
          reader.readAsDataURL(event.target.files[i]); 
          this.myFilesNew19.push(event.target.files[i]);
        }
      }
    }

    
  }
  removeImage(url:any,position_number:any,imgNumber:any){
    //console.log(this.images,url);
    //console.log(this.myFiles);
    console.log("here remove");
    console.log("position_number ",position_number);
    console.log("imgNumber ",imgNumber);
    if(position_number == 0)
    {
      this.images0 = this.images0.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew0.splice(imgNumber,1);
    }
    if(position_number == 1)
    {
      this.images1 = this.images1.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew1.splice(imgNumber,1);
    }
    
    if(position_number == 2)
    {
      this.images2 = this.images2.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew2.splice(imgNumber,1);
    }
      
    if(position_number == 3)
    {
      this.images3 = this.images3.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew3.splice(imgNumber,1);
    }
    if(position_number == 4)
    {
      this.images4 = this.images4.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew4.splice(imgNumber,1);
    }
    if(position_number == 5)
    {
      this.images5 = this.images5.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew5.splice(imgNumber,1);
    }
    if(position_number == 6)
    {
      this.images6 = this.images6.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew6.splice(imgNumber,1);
    }
    if(position_number == 7)
    {
      this.images7 = this.images7.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew7.splice(imgNumber,1);
    }
    if(position_number == 8)
    {
      this.images8 = this.images8.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew8.splice(imgNumber,1);
    }
    if(position_number == 9)
    {
      this.images9 = this.images9.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew9.splice(imgNumber,1);
    }

    if(position_number == 10)
    {
      this.images10 = this.images10.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew10.splice(imgNumber,1);
    }
    if(position_number == 11)
    {
      this.images11 = this.images11.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew11.splice(imgNumber,1);
    }
    if(position_number == 12)
    {
      this.images12 = this.images12.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew12.splice(imgNumber,1);
    }
    if(position_number == 13)
    {
      this.images13 = this.images13.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew13.splice(imgNumber,1);
    }
    if(position_number == 14)
    {
      this.images14 = this.images14.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew14.splice(imgNumber,1);
    }
    if(position_number == 15)
    {
      this.images15 = this.images15.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew15.splice(imgNumber,1);
    }
    if(position_number == 16)
    {
      this.images16 = this.images16.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew16.splice(imgNumber,1);
    }
    if(position_number == 17)
    {
      this.images17 = this.images17.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew17.splice(imgNumber,1);
    }
    if(position_number == 18)
    {
      this.images18 = this.images18.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew18.splice(imgNumber,1);
    }
    if(position_number == 19)
    {
      this.images19 = this.images19.filter(img => (img != url));
      //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
      ///this.patchValues();
      //console.log(imgNumber);
      this.myFilesNew19.splice(imgNumber,1);
    }

  }
  quantities() : FormArray {
    return this.productForm.get("quantities") as FormArray
  }
  quantitiesMonday() : FormArray {
    return this.productForm.get("quantitiesMonday") as FormArray
  }

  quantitiesTuesday() : FormArray {
    return this.productForm.get("quantitiesTuesday") as FormArray
  }

  newQuantity(): FormGroup {
    return this.fb.group({
      firstSubVariation: '',
      firstSubImage: '',
      color: '',
      size: '',
      price: '',
      stock: '',
      sku: '',
    })
  }
  newQuantity2(): FormGroup {
    return this.fb.group({
      old_firstSubVariation: '',
      old_firstSubImage: '',
      old_color: '',
      old_size: '',
      old_price: '',
      old_stock: '',
      old_sku: '',
    })
  }
  newQuantity3(): FormGroup {
    return this.fb.group({
      firstSubVariation: '',
      firstSubImage: '',
      old_variation_id:'',
      old_images:'',
      old_color: '',
      old_size: '',
      old_price: '',
      old_stock: '',
      old_sku: '',
    })
  }

  addQuantity() {
    this.add_quantity_number++;
    this.quantities().push(this.newQuantity());
  }
  addQuantityMonday() 
  {
    console.log("monday call");
    this.add_quantity_monday_number++;
    this.quantitiesMonday().push(this.newQuantity2());
  }
  addQuantityTuesday() {
    console.log("hereeeeeeeeeeeee 923 ");
    this.quantitiesTuesday().push(this.newQuantity3());
  }
  

  removeQuantity(i:number) {

    this.add_quantity_number--;
    this.quantities().removeAt(i);
  }
  removeQuantityMonday(i:number) {
    this.add_quantity_monday_number--;
    this.quantitiesMonday().removeAt(i);
  }
  removeQuantityTuesday(i:number) {
    this.quantitiesTuesday().removeAt(i);
  }

  ngOnInit(): void {
    this.getCategories();

  }

  onSubmit()
  { 
      

    if (this.productForm.valid)
    {
      this.loading = true
      this.regdis = true
      console.log("form sumit");
      console.log(this.productForm.value);
      console.log("firstMainVariationVal   --->>>>> "+this.firstMainVariationVal);
      console.log("secondMainVariationVal   --->>>>> "+this.secondMainVariationVal);
      console.log("firstMainVariationVal   --->>>>> "+this.firstMainVariationVal);
      console.log("firstSubVariationArray "+this.firstSubVariationArray);
      console.log("secondSubVariationArray "+this.secondSubVariationArray);
      console.log("myFilesNew0 "+this.myFilesNew0);
      
      this.formData = new FormData(); 
      this.formData.append('id', this.id); 
        this.formData.append('firstMainVariationVal', this.firstMainVariationVal); 
        this.formData.append('secondMainVariationVal', this.secondMainVariationVal);
        this.formData.append('firstSubVariationArray', this.firstSubVariationArray);
        this.formData.append('secondSubVariationArray', this.secondSubVariationArray);
        this.formData.append('formValue', JSON.stringify(this.productForm.value));
        for (var i = 0; i < this.myFilesNew0.length; i++)
        { 
          this.formData.append("image0", this.myFilesNew0[i]);
        }
        for (var i = 0; i < this.myFilesNew1.length; i++)
        { 
          this.formData.append("image1", this.myFilesNew1[i]);
        }
        for (var i = 0; i < this.myFilesNew2.length; i++)
        { 
          this.formData.append("image2", this.myFilesNew2[i]);
        }
        for (var i = 0; i < this.myFilesNew3.length; i++)
        { 
          this.formData.append("image3", this.myFilesNew3[i]);
        }
        for (var i = 0; i < this.myFilesNew4.length; i++)
        { 
          this.formData.append("image4", this.myFilesNew4[i]);
        }
        for (var i = 0; i < this.myFilesNew5.length; i++)
        { 
          this.formData.append("image5", this.myFilesNew5[i]);
        }
        for (var i = 0; i < this.myFilesNew6.length; i++)
        { 
          this.formData.append("image6", this.myFilesNew6[i]);
        }
        for (var i = 0; i < this.myFilesNew7.length; i++)
        { 
          this.formData.append("image7", this.myFilesNew7[i]);
        }
        for (var i = 0; i < this.myFilesNew8.length; i++)
        { 
          this.formData.append("image8", this.myFilesNew8[i]);
        }
        for (var i = 0; i < this.myFilesNew9.length; i++)
        { 
          this.formData.append("image9", this.myFilesNew9[i]);
        }
        for (var i = 0; i < this.myFilesNew10.length; i++)
        { 
          this.formData.append("image10", this.myFilesNew10[i]);
        }
        for (var i = 0; i < this.myFilesNew11.length; i++)
        { 
          this.formData.append("image11", this.myFilesNew11[i]);
        }
        for (var i = 0; i < this.myFilesNew12.length; i++)
        { 
          this.formData.append("image12", this.myFilesNew12[i]);
        }
        for (var i = 0; i < this.myFilesNew13.length; i++)
        { 
          this.formData.append("image13", this.myFilesNew13[i]);
        }
        for (var i = 0; i < this.myFilesNew14.length; i++)
        { 
          this.formData.append("image14", this.myFilesNew14[i]);
        }
        for (var i = 0; i < this.myFilesNew15.length; i++)
        { 
          this.formData.append("image15", this.myFilesNew15[i]);
        }
        for (var i = 0; i < this.myFilesNew16.length; i++)
        { 
          this.formData.append("image16", this.myFilesNew16[i]);
        }
        for (var i = 0; i < this.myFilesNew17.length; i++)
        { 
          this.formData.append("image17", this.myFilesNew17[i]);
        }
        for (var i = 0; i < this.myFilesNew18.length; i++)
        { 
          this.formData.append("image18", this.myFilesNew18[i]);
        }
        for (var i = 0; i < this.myFilesNew19.length; i++)
        { 
          this.formData.append("image19", this.myFilesNew19[i]);
        }
        this.http.post(this.base_url+"api/product/webUpdateSubmitAttribute",this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse_2 = response;
          if(this.apiResponse_2.status == true)
          {
            setTimeout(() => {
              window.location.href = this.base_url_front+"dashboard/(seller:myproducts)/";
            }, 2000); 
          }
        });
    }else{
      this.validateAllFormFields(this.productForm); 
    }
  }

  get f(){
    return this.productForm.controls;
  }
  
  validateAllFormFields(formGroup: FormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  addAttributeClick()
  {
    console.log("click here");
    console.log(this.productForm.value);
    this.variationNameOne = this.productForm.value.firstMainVariation;
    this.variationNameTwo = this.productForm.value.secondMainVariation;
    this.subVariationNameOne = this.productForm.value.quantities;
    this.subVariationNameTwo = this.productForm.value.quantitiesMonday;

    // console.log("variationNameOne   --->>>>> "+this.variationNameOne);
    // console.log("subVariationNameOne   --->>>>> "+this.subVariationNameOne);
    // console.log("variationNameTwo   --->>>>> "+this.variationNameTwo);
    // console.log("subVariationNameTwo   --->>>>> "+this.subVariationNameTwo);
    
    // console.log("this.productForm.value.quantities "+this.productForm.value.quantities);
    // console.log("this.productForm.value.quantities[0] "+this.productForm.value.quantities[0]);
    // console.log("this.productForm.value.quantities[0].firstSubVariation "+this.productForm.value.quantities[0].firstSubVariation);
    
    this.firstMainVariationVal = this.productForm.value.firstMainVariation;
    this.secondMainVariationVal = this.productForm.value.secondMainVariation;
    
    for(let x=0; x<this.productForm.value.quantities.length; x++)
    {
      //console.log("this.productForm.value.quantities[x].firstSubVariation "+this.productForm.value.quantities[x].firstSubVariation);
      this.firstSubVariationArray.push(this.productForm.value.quantities[x].firstSubVariation);
    }
    for(let y=0; y<this.productForm.value.quantitiesMonday.length; y++)
    {
      //console.log("this.productForm.value.quantitiesMonday[y].secondSubVariation "+this.productForm.value.quantitiesMonday[y].secondSubVariation);
      this.secondSubVariationArray.push(this.productForm.value.quantitiesMonday[y].secondSubVariation);
    }
    console.log("firstSubVariationArray "+this.firstSubVariationArray);
    console.log("secondSubVariationArray "+this.secondSubVariationArray);
    
  }
  applyPSS() 
  {
    
  }
  onFileUpdate(event: any, index: any) {
    this.myFiles = [];
    const files = event.target.files;

    if (files.length === 0) return;

    const reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onload = _event => {

      this.products[index].imgBase64Data = reader.result as string;
      // this.myFiles.push(event.target.files);
      // console.log(this.myFiles)
      this.allpics.push(event.target.files)
      // console.log(this.allpics)


    };
  }
  onSelectFile(event: any) {
    const file = event.target.files && event.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      if (file.type.indexOf('image') > -1) {
        this.format = 'image';
      } else if (file.type.indexOf('video') > -1) {
        this.format = 'video';
      }
      reader.onload = (event) => {
        this.url = (<FileReader>event.target).result;
      }
    }
  }

  addPhone(): void {
    (this.userForm.get('phones') as FormArray).push(
      this.fb.control(null)
    );
    
  }

  addPhone2(){
    
    this.mytestarray.push({name:"test"})
    console.log(this.mytestarray);

    (this.userForm.get('phones2') as FormArray).push(this.fb.control(null));
   
  }
  

  removePhone(index: any) {
    (this.userForm.get('phones') as FormArray).removeAt(index);
  }

  removePhone2(index: any) {
    (this.userForm.get('phones2') as FormArray).removeAt(index);
  }

  getPhonesFormControls(): AbstractControl[] {
   
    return (<FormArray>this.userForm.get('phones')).controls
  }

  getPhonesFormControls2(): AbstractControl[] {
    
    return (<FormArray>this.userForm.get('phones2')).controls
  }

  send(values: any) {

    this.mydispatch_info = {
      "weight": this.weight.value,
      "size":
        { "width": this.width.value, "Length": this.length.value, "Height": this.height.value },
      "shipping_details": {
        "some": "one"
      }
    }
    this.mysize = { "width": this.width.value, "Length": this.length.value, "Height": this.height.value }
    this.form.value.photo = this.allpics
    this.form.value.dispatch_info = this.mydispatch_info
    this.form.value.size = this.mysize
    this.form.value.other_details = this.addSection5.value
    console.log(this.form.value)
    // this.http.post(this.baseurl+"api/product/createproduct", this.form.value).subscribe(res => {
    //   console.log(res)
    //   alert("Product Saved succesfully")
    //   window.location.href='/dashboard/(seller:myproducts)'
    //  // this.router.navigate(['myproducts'])

    //   console.log(res)
    // })

  }
  sendWeight(values: any) {
    console.log(this.weightval)

    console.log(values.currentTarget.checked);
    if (values.currentTarget.checked) {
      this.http.post(this.baseurl + "api/shipping/getshippingMethodsByweight", { "weight": this.weightval }).subscribe(res => {
        this.getshippingMethodsByweight = res
        this.recordsship = this.getshippingMethodsByweight.data
        console.log(this.recordsship)

      })
    }
    else {
      this.recordsship = []
    }

  }
  weightHere(weight: any) {
    this.weightval = parseInt(weight.target.value)
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CategoriesComponent, {
      height: '600px',
      width: '700px',
      position: {
        top: '12vh',
        left: '30vw'
      },
    });
  }
  getCategories() {
    this.http.get(this.baseurl + "api/product/getallcatagoriesflat").subscribe(res => {
      this.allCategories = res
      const categories = this.allCategories.data.filter((x: any) => {

        return x.mastercatagory == null
      })
      if (categories) {
        this.parentCategories = categories
        console.log(this.parentCategories, "test")
      }
    })
  }
  onSelect(id:any){
    
    const categories = this.allCategories.data.filter((x: any) => {
      return x.mastercatagory == id.target.value
    })
    if (categories) {
      this.subparentCategories = categories
      console.log(categories, "test2")
    }
  }
  simpleView(){
    this.simplecontent = false
    this.variablecontent = true
  }
  variableView(){
    this.simplecontent = true
    this.variablecontent = false
  }

  getVariations(variation:any){
    console.log(variation.target.value)
    this.myvariation  = variation.target.value
  }
  onKeydownEvent(eve:any){
    console.log(eve)
  }
}

