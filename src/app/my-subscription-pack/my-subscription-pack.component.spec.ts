import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MySubscriptionPackComponent } from './my-subscription-pack.component';

describe('MySubscriptionPackComponent', () => {
  let component: MySubscriptionPackComponent;
  let fixture: ComponentFixture<MySubscriptionPackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MySubscriptionPackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MySubscriptionPackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
