import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-your-personal',
  templateUrl: './your-personal.component.html',
  styleUrls: ['./your-personal.component.css']
})
export class YourPersonalComponent implements OnInit {
  about:any;
  aboutdata:any
  baseurl:any
  constructor(private seller:SellerserviceService, private http:HttpClient) {
    this.baseurl = seller.baseapiurl2
   }

  ngOnInit(): void {
    this.http.get(this.baseurl +"api/common/get_page_from_slug/seller_web_privacy_policy" ).subscribe(res=>{
      this.about = res
      this.aboutdata = this.about.data
     console.log(this.aboutdata)
    })
  }

}
