import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-get-schedule',
  templateUrl: './get-schedule.component.html',
  styleUrls: ['./get-schedule.component.css']
})
export class GetScheduleComponent implements OnInit {
  baseurl: any
  ordersarray: any
  orders: any;
  myoptions: any; record_2:any;  record_3:any;
  main_id: any
  searchText: any; APIres: any
  categoryoptions:any
  catagory:any;condition:any; title:any; record:any;
  getallcatagoriesflat:any; getallcatagoriesflatRes:any; allCat:any
  
  constructor(private seller: SellerserviceService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router)
    {
      this.baseurl = seller.baseapiurl2
      this.ordersarray = []
      this.orders = []
      this.main_id = localStorage.getItem('main_sellerid')
      
      this.getData();

    }
  getData()
  {
    this.http.get(this.baseurl+"api/seller/all_getTimeSloatSeller/"+this.main_id).subscribe(res => {
      this.ordersarray = res;
      
      if(this.ordersarray.status == true)
      {
        this.record = this.ordersarray.record;
        this.record_2 = this.ordersarray.record_2;
        this.record_3 = this.ordersarray.record_3;
      }
      
      //this.orders = this.ordersarray.data
      console.log("order ", res);
    })
  }  
  ngOnInit(): void { }

  myRadioChange(val:any)
  {
    //alert(val);
    if(confirm("Êtes-vous sûr de vouloir effectuer cette action ?"))
    {
      this.http.get(this.baseurl+"api/seller/enableDisableTimeSloat/"+val).subscribe((val)=>{
        console.log(val);
        this.ordersarray = val;
        if(this.ordersarray.status == true)
        {
          
        }
        this.getData();
      })
    }
    //window.location.reload();
  }
  deleteSchedule(event:any,val_id:any)
  {
    console.log("val_id " , val_id);
    if(confirm("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))
    {
      console.log("ifffffffffffffffff " , val_id);
      this.http.get(this.baseurl+"api/seller/deleteTimeSloatSeller/"+val_id ).subscribe(res => {
        this.ordersarray = res;
        if(this.ordersarray.status == true)
        {
          //window.location.reload();
        }
        this.getData();
        //this.orders = this.ordersarray.data
        console.log("order ", res);
      })
    }
  }

}
