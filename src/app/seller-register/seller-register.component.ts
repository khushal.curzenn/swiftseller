import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { WaitMseegeComponent } from '../wait-mseege/wait-mseege.component';
import { ConfirmRegisterComponent } from '../confirm-register/confirm-register.component';

@Component({
  selector: 'app-seller-register',
  templateUrl: './seller-register.component.html',
  styleUrls: ['./seller-register.component.css']
})
export class SellerRegisterComponent implements OnInit {
  interests: any
  isLinear: any
  baseurl: any
  getstatus: any
  mainid: any
  registerform !: FormGroup
  registerform1 !: FormGroup
  myfullname: any
  myemail: any
  mypassword: any; showmymodal = false
  messege: any
  endAddress: any; endLatitude: any; endLongitude: any; bankAddress: any
  mycountrycode: any; myphone: any; getSubscriptionUrl: any
  getSubscriptiondata: any; createSubscriptionoptions: any
  mainSubscriptionURL: any; mainSubAPIres: any; myFiles: string[] = [];
  myFilesf: string[] = []; myFilesb: string[] = [];
  mypickaddress: any;   registrationFiles: string[] = [];
  Finalregisterform: any
  notindiviual: string="";  registrationFilesShow:any;
  forntimage: any; backimage: any; shopimage: any

  mystep: any
  pickadderr: any
  bankadderr: any

  getuserprofile: any; getuserprofileRes: any;singlePro:any
  documentTypeVar:string=""; front_url:string="";
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute, private toastr: ToastrService
    , private dialog: MatDialog) {
    this.interests = []
    this.getSubscriptiondata = []
    this.mainid = this.activateroute.snapshot.params['id']

    this.front_url = seller.hosturl;
    this.baseurl = seller.baseapiurl2;
    this.getSubscriptionUrl = this.baseurl + 'api/subs/getAllSubscriptions'
    this.mainSubscriptionURL = this.baseurl + 'api/subs/createSubscription'
    this.getuserprofile = this.baseurl + 'api/seller/getuserprofile/' + this.mainid
    //this.notindiviual = true
    this.pickadderr = true
    this.bankadderr = true
    this.mypassword = ''
    this.myemail = ''
    this.myfullname = ''
    this.mycountrycode = ''
    this.myphone = ''

    this.http.get(this.getuserprofile).subscribe(res => {
      this.getuserprofileRes = res
      if (this.getuserprofileRes.status) {
        this.singlePro = this.getuserprofileRes.data
       
        this.mypassword =  ''
        this.myemail = this.singlePro.email
        this.myfullname = this.singlePro.fullname
        this.mycountrycode = this.singlePro.countrycode
        this.myphone = this.singlePro.phone
        
      
      }
    })
    this.registerform = new FormGroup({

      vendortype: new FormControl('', Validators.required),
      companyname: new FormControl('',),
     // ICorPassport: new FormControl('', Validators.required),
      documentType: new FormControl('', Validators.required),
      company_registration_number: new FormControl('',),
      shopname: new FormControl('', Validators.required),
      shopdesc: new FormControl(''),

      email: new FormControl(this.myemail),
      countrycode: new FormControl(this.mycountrycode),
      phone: new FormControl(this.myphone),
      fullname: new FormControl(this.myfullname),


      bic: new FormControl('', Validators.required),
      iban: new FormControl('', Validators.required),
      bank_name: new FormControl('', Validators.required),
      account_name: new FormControl('', Validators.required),

      password: new FormControl(this.mypassword),


    })

    


    this.createSubscriptionoptions = {
      subscriber_name: this.myfullname,
      subscriber_email: this.myemail,
      provider_id: this.mainid
    }
   
    this.mypickaddress = {

    }
    this.mystep = ''
  }

  ngOnInit() {
    this.http.get(this.getSubscriptionUrl).subscribe(res => {
      this.getSubscriptiondata = res
    })
  }

  firststep() 
  {
    
    console.log("vendortype " , this.registerform.value.vendortype);
    if(this.registerform.value.vendortype == "")
    {
      this.registerform.controls['vendortype'].markAsTouched();
      this.registerform.controls['documentType'].markAsTouched();
      
    }else if(this.registerform.value.documentType == "")
    {
      this.registerform.controls['documentType'].markAsTouched();
    }else{
      this.mystep = 'step-1';
    }
    

    // if (this.registerform.controls['ICorPassport'].status == "VALID" && this.registerform.controls['vendortype'].status == "VALID") {
    //   this.mystep = 'step-1'
    // } else {
    //   this.registerform.controls['ICorPassport'].markAsTouched();
    //   this.registerform.controls['vendortype'].markAsTouched();
    // }

    
  }
  handleAddressChange2(address2: any) {

    this.endAddress = address2.formatted_address
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
    this.mypickaddress.address = this.endAddress
    this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]


  }
  handleAddressChange(bankAddress: any) {
    console.log("hereeee");
    this.bankAddress = bankAddress.formatted_address
  }
  sendSubscription(subpac: any) {
    this.createSubscriptionoptions.name_of_package = subpac.package
    this.createSubscriptionoptions.duration = subpac.duration
    this.createSubscriptionoptions.package_id = subpac._id
  }

  onFileUpdate(event: any) {
    this.myFiles = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
    //  console.log(this.myFiles)

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.shopimage = reader.result
      //console.log(reader.result );
    };
  }
  onFileUpdatefront(event: any): void {

    this.myFilesf = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFilesf.push(event.target.files[i]);
    }
    // console.log(this.myFilesf)


    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.forntimage = reader.result
      //console.log(reader.result );
    };

  }

  onFileUpdateback(event: any) {
    this.myFilesb = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFilesb.push(event.target.files[i]);
    }
    // console.log(this.myFilesb)

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.backimage = reader.result
      //console.log(reader.result );
    };
  }

  onFileUploadRegistrationNumber(event: any) {
    this.registrationFiles = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.registrationFiles.push(event.target.files[i]);
    } 

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.registrationFilesShow = reader.result
      //console.log(reader.result );
    };
  }

  register() {
   
    
    console.log("this.registerform.value.company_registration_number  " , this.registerform.value.company_registration_number );


    //return false;


    this.registerform.value.pickupaddress = this.mypickaddress
    this.Finalregisterform = new FormData();
    this.Finalregisterform.append('vendortype', this.registerform.value.vendortype);
    this.Finalregisterform.append('ICorPassport', null);
    this.Finalregisterform.append('companyname', this.registerform.value.companyname);
    this.Finalregisterform.append('company_registration_number', this.registerform.value.company_registration_number);
    
    this.Finalregisterform.append('shopname', this.registerform.value.shopname);
    this.Finalregisterform.append('shopdesc', this.registerform.value.shopdesc);
    for (var i = 0; i < this.myFiles.length; i++) {
      this.Finalregisterform.append("shopphoto", this.myFiles[i]);
    }
    for (var i = 0; i < this.myFilesf.length; i++) {
      this.Finalregisterform.append("front", this.myFilesf[i]);
    }
    for (var i = 0; i < this.myFilesb.length; i++) {
      this.Finalregisterform.append("back", this.myFilesb[i]);
    }
    for (var i = 0; i < this.registrationFiles.length; i++) {
      this.Finalregisterform.append("registration_card", this.registrationFiles[i]);
    }

    

    this.Finalregisterform.append('fullname', this.myfullname);
    this.Finalregisterform.append('email', this.myemail);
    this.Finalregisterform.append('phone', this.myphone);
    this.Finalregisterform.append('countrycode', this.mycountrycode);
    this.Finalregisterform.append('pickupaddress', JSON.stringify(this.registerform.value.pickupaddress));

    this.Finalregisterform.append('shopAddress', this.endAddress );
    this.Finalregisterform.append('shopLatitude', this.endLatitude );
    this.Finalregisterform.append('shopLongitude', this.endLongitude );

    this.Finalregisterform.append('bank_address', JSON.stringify(this.bankAddress));
    this.Finalregisterform.append('bic', this.registerform.value.bic);
    this.Finalregisterform.append('iban', this.registerform.value.iban);
    this.Finalregisterform.append('bank_name', this.registerform.value.bank_name);
    this.Finalregisterform.append('account_name', this.registerform.value.account_name);
    this.Finalregisterform.append('password',  this.mypassword);
    this.Finalregisterform.append('id', this.mainid);
    this.Finalregisterform.append('web_reg_step', 4);
    this.Finalregisterform.append('is_registered', true);






    //form valid
    // if (this.registerform.valid == true) {}
    // else {
    //   for (const control of Object.keys(this.registerform.controls)) {
    //     this.registerform.controls[control].markAsTouched();
    //   }
    //   return;
    // }
      // this.createSubscriptionoptions.subscriber_name =  this.myfullname
      // this.createSubscriptionoptions.subscriber_email = this.myemail
      // this.http.post(this.mainSubscriptionURL, this.createSubscriptionoptions).subscribe(res => { })
      //   this.mainSubAPIres = res
      //   if (this.mainSubAPIres.status == true) {} else {
      //     // this.toastr.error(this.mainSubAPIres.message, 'Error');
      //   }

          this.http.post(this.baseurl + "api/seller/createseller_second_update", this.Finalregisterform).subscribe(res => {
            this.getstatus = res;
            console.log("this.getstatus ",this.getstatus);
            if (this.getstatus.status == true)
            {
              window.location.href = this.front_url+"login";
            } else {
              // this.toastr.error(this.getstatus.errmessage, 'Error');
            }
          })



        
     
    
  }


  // getEnterprise(eve: any) {
  //   this.notindiviual = false
  // }
  getindivisual(eve: any) {
    this.notindiviual = eve.target.value;
    console.log("this.notindiviual " , this.notindiviual );
  }

  documentTypeFun(eve: any) 
  {
    this.documentTypeVar = eve.target.value;
    console.log("documentTypeFun " , eve.target.value)
  }
  
  secondstep() {

    if (this.registerform.controls['shopname'].status == "VALID" && this.mypickaddress.address != undefined) 
    {
      this.mystep = 'step-2'
      this.pickadderr = true
    } else {
      this.registerform.controls['shopname'].markAsTouched();
      this.pickadderr = false
    }

  }
  thirdstep() {

    if (this.registerform.controls['bic'].status == "VALID" && this.registerform.controls['iban'].status == "VALID" &&
      this.registerform.controls['bank_name'].status == "VALID" && this.registerform.controls['account_name'].status == "VALID" && this.bankAddress != undefined) {
      this.mystep = 'step-3'
      this.bankadderr = true

    } else {
      this.registerform.controls['bic'].markAsTouched();
      this.registerform.controls['iban'].markAsTouched();
      this.registerform.controls['bank_name'].markAsTouched();
      this.registerform.controls['account_name'].markAsTouched();

      this.bankadderr = false
    }
  }
}
