import { Component, OnInit } from '@angular/core';
import { AddBankComponent } from '../add-bank/add-bank.component';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr'
@Component({
  selector: 'app-bankaccount',
  templateUrl: './bankaccount.component.html',
  styleUrls: ['./bankaccount.component.css']
})
export class BankaccountComponent implements OnInit {
  baseurl:any;main_id:any; allBanks:any;main_email:any;myOptions:any;APIres:any

  getSingleBankDetail:any; getSingleBankDetailRes:any; singleBank:any

  registerform!: FormGroup
  bankaddress:any; endAddress:any; endLatitude:any; endLongitude:any;
  updateBankDetail:any; updateBankDetailRes:any
  
  constructor(public dialog: MatDialog,private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute, public toastr: ToastrService) { 
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')
      this.main_email = localStorage.getItem('selleremail')
      this.getSingleBankDetail = this.baseurl+"api/seller/getSingleBankDetail"
      this.updateBankDetail = this.baseurl+"api/seller/updateBankDetail"
      this.allBanks =[]
      this.myOptions ={
        email:this.main_email,
       // bank_id:this.main_id 
      }

      this.registerform = new FormGroup({       
        bic:new FormControl('', Validators.required),
        iban:new FormControl('', Validators.required),
        bank_name: new FormControl('', Validators.required),
        account_number:new FormControl('', Validators.required),
        account_name:new FormControl('', Validators.required),
        bank_address:new FormControl('', Validators.required)
       })
       this.bankaddress = {

       }
    }

  ngOnInit(): void {
    this.getBanks()
  }
  getBanks(){
    this.http.get(this.baseurl +"api/seller/getallbanks/"+this.main_id).subscribe(res=>{
      this.allBanks = res
      console.log(this.allBanks)
      
    })
  }
  deleteAccount(id:any){
    this.myOptions.bank_id = id
    console.log(this.myOptions)
    var DywDelete = confirm("Voulez-vous supprimer cette banque ?")
    if(DywDelete == true){
    this.http.post(this.baseurl +"api/seller/deletebank", this.myOptions).subscribe(res=>{
      this.APIres =  res
      if(this.APIres.status == true){
       
        this.toastr.success(this.APIres.message, "Success", {
          timeOut: 3000,
        })
        window.location.reload()
      }
    })
  }
    
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddBankComponent ,{
      height: '600px',
      width: '700px',
      position: {
        top: '16vh',
        left: '30vw'
    },
    });
  }

  handleAddressChange2(address2: any) {

    this.endAddress = address2.formatted_address
    console.log( this.endAddress )
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
    this.bankaddress.address = this.endAddress
    this.bankaddress.latlong = [this.endLatitude, this.endLongitude]


  }

  getCoupon(bankid:any){

    const params={
       "seller_id":this.main_id,
       "bank_id":bankid
    }
    this.http.post(this.getSingleBankDetail, params).subscribe(res=>{
      this.getSingleBankDetailRes = res
    
      if(this.getSingleBankDetailRes.status){
        this.singleBank = this.getSingleBankDetailRes.data[0]
       
        this.bankaddress.address = this.singleBank.bank_address
        this.bankaddress.latlong = this.singleBank.latlong
       
        this.registerform = new FormGroup({       
          bic:new FormControl(this.singleBank.bic , Validators.required),
          iban:new FormControl(this.singleBank.iban, Validators.required),
          bank_name: new FormControl(this.singleBank.bank_name, Validators.required),
          account_number:new FormControl(this.singleBank.account_number, Validators.required),
          bank_address:new FormControl(this.singleBank.bank_address, Validators.required)
          
         })

      }
    })
    //console.log(bankid)
  }

  sendBank(){

    this.registerform.value.bank_address = this.bankaddress
    this.registerform.value.seller_id = this.main_id
    this.registerform.value.email = this.main_email
    this.registerform.value.bank_id = this.singleBank._id
    
    if(this.registerform.valid){

      this.http.post(this.updateBankDetail, this.registerform.value).subscribe(res=>{
        this.updateBankDetailRes = res
        if(this.updateBankDetailRes.status){
          window.location.reload()
        }
      })

    }else{
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }

  }

}
