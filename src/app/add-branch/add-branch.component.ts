import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SellerserviceService } from '../sellerservice.service';

@Component({
  selector: 'app-add-branch',
  templateUrl: './add-branch.component.html',
  styleUrls: ['./add-branch.component.css']
})
export class AddBranchComponent implements OnInit {

  registerform!: FormGroup
  baseurl:any;main_id:any;getstatus:any;main_email:any
  endAddress:any; endLatitude:any; endLongitude:any; mypickaddress:any
  formData:any;
  myFiles:string [] = [];
  constructor(private seller:SellerserviceService, private http:HttpClient)
  {
    this.baseurl = seller.baseapiurl2
    this.main_id = localStorage.getItem('main_sellerid')
    this.main_email = localStorage.getItem('selleremail');

    this.registerform = new FormGroup({       
      shopname:new FormControl('', Validators.required),
      shopdesc:new FormControl('', Validators.required),
      address: new FormControl('', Validators.required)
        
     })


  }

  ngOnInit(): void {
    this.myFiles = [];
  }
  handleAddressChange2(address2: any) {

    this.endAddress = address2.formatted_address
    console.log( this.endAddress )
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
    console.log( this.endLatitude );
    console.log( this.endLongitude );
    //this.mypickaddress.address = this.endAddress
    //this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]
  }

  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }


  sendBank(){
    this.registerform.value.seller_id = this.main_id
    this.registerform.value.email = this.main_email
    //this.registerform.value.bank_address = this.mypickaddress
    this.registerform.value.address = this.endAddress;
    this.registerform.value.shopLatitude = this.endLatitude;
    this.registerform.value.shopLongitude = this.endLongitude;
    
    this.formData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++)
    { 
      this.formData.append("shopphoto", this.myFiles[i]);
    }
    this.formData.append('seller_id', this.main_id );
    this.formData.append('email', this.main_email );
    this.formData.append('address', this.endAddress);
    this.formData.append('shopLatitude', this.endLatitude);
    this.formData.append('shopLongitude', this.endLongitude);
    this.formData.append('shopdesc', this.registerform.value.shopdesc);
    this.formData.append('shopname', this.registerform.value.shopname);
    
    if(this.registerform.valid){
      this.http.post(this.baseurl+"api/seller/addBranch" , this.formData ).subscribe(res=>{
        this.getstatus =res
        if(this.getstatus.status == true){
          // alert(this.getstatus.message)
          window.location.reload()
        }
      })
    }else{
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }
    
  }

}
